import awkward as ak
from coffea.analysis_tools import PackedSelection


class njetsTagger:

    def __init__(self) -> None:
        pass

    @property
    def name(self) -> str:
        return "njetsTagger"

    # lower priority is better
    # first decimal point is category within tag (in case for untagged)
    @property
    def priority(self) -> int:
        return 20

    def GetCategory(self, njets) -> int:

        if njets is None:
            return None
        else:

            if njets < 2:
                cat = njets
            else:
                cat = 2

            return cat

    def __call__(self, events: ak.Array, diphotons: ak.Array) -> ak.Array:
        """
        We can classify events according to it's njets of diphotons.
        """

        selection = PackedSelection()

        selection.add("n_iso_photon", ak.num(events.GenIsolatedPhoton, axis=1) > 1)

        events = events.mask[selection.all("n_iso_photon")]
        lead_pho = events.GenIsolatedPhoton[:, 0]
        sublead_pho = events.GenIsolatedPhoton[:, 1]
        # diphoton = (lead_pho) + (sublead_pho)

        genjet = events.GenJet

        ## pt and eta selection
        pt_cut = genjet.pt > 30
        eta_cut = abs(genjet.eta) < 2.5
        genjet = genjet[(pt_cut) & (eta_cut)]

        # genjet_eta = genjet.eta
        # genjet_phi = genjet.phi

        ## clean with isolated photon
        mask_1 = genjet.delta_r(lead_pho) > 0.4
        mask_2 = genjet.delta_r(sublead_pho) > 0.4

        clean_genjet_mass = genjet.mass.mask[mask_1]
        clean_genjet_mass = clean_genjet_mass.mask[mask_2]

        ## remove none
        non_none_mask = clean_genjet_mass is not None

        self.njets = ak.sum(non_none_mask, axis=-1)

        cat_vals = ak.Array(map(self.GetCategory, self.njets))
        cats_by_diphoEvt = self.priority + cat_vals

        return (cats_by_diphoEvt, {})
